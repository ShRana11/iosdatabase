//
//  ViewController.swift
//  DayTwo
//
//  Created by MacStudent on 2018-11-09.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CoreData




class ViewController: UIViewController {
  var context:NSManagedObjectContext!
    
    @IBOutlet weak var nameText: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        print("add pressed")
        let name = nameText.text!
        var p = User(context: self.context)
        p.age = 40
        p.name = name
        
        do{
            try self.context.save();
        }catch{
            print("error while saving")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("step 1 done")
        let screen2 = segue.destination as! screen2VC
        screen2.person = nameText.text!
        
    }
    @IBAction func loadButtonPressed(_ sender: Any) {
        print("load pressed")
        // This is the same as:  SELECT * FROM User
        let fetchRequest:NSFetchRequest<User> = User.fetchRequest()
        /// add where clause .. use nspreedate for this__> whwer name == jenelle
        fetchRequest.predicate = NSPredicate(format: "name == %@", "jenelle")
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [User]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("User Name: \(x.name)")
                print("User Age: \(x.age)")
            }
        }
        catch {
            print("Error when fetching from database")
        }
    }
}

